# Präsentationen mit Beamer

Mein Theme für Beamer-Präsentationen und Handouts.

## Benötigte Software

```
sudo apt install pandoc texlive-latex-recommended texlive-latex-extra
```

## Anleitung

Die eigentliche Präsentation ist in [source.md](source.md).

* `make`: Slides und Handout erstellen
* `make slides`: Slides erstellen
* `make handout`: Handout erstellen

Die pdf-Dateien werden im Ordner `output` erstellt.




