all:	slides handout

slides:
	mkdir -p output
	pandoc -t beamer source.md -o output/slides.pdf -V theme=lintel -V colortheme=lintel -V aspectratio=1610 --highlight-style kate
	
handout:
	mkdir -p output
	pandoc source.md -H handout.tex -o output/handout.pdf