---
title: "Präsentationen mit Beamer"
author: "Christian Künne"
institute: "#linuxzeugs"
date: "15/09/2019"
---

# Inhalt

* Ein wenig Markdown
* Listen und Aufzählungen
* Tabellen
* Bilder
* Spalten
* Code
* Pausen
* Notizen
* Textausrichtung

# Ein wenig Markdown

## Fett und kursiv

Hier steht Text, der **fett**, *kursiv* oder ***fett kursiv*** sein kann.

Man kann Text auch \alert{rot} hervorheben.

## Links

Das ist ein [Link](https://github.com/josephwright/beamer) zum Beamer Repository.

## Fußnoten

\LaTeX\ wurde Anfang der 1980er Jahre von Leslie Lamport^[Leslie Lamport: LaTeX: A Document Preparation System] entwickelt.

# Listen und Aufzählungen

* Das
* ist
* eine
* Liste
   * mit
   * Unterpunkten
      * und
      * Unter-unterpunkten

1. Das
2. ist
3. eine
4. Aufzählung
   1. mit
   2. Unterpunkten
      1. und
      2. Unter-unterpunkten

# Tabellen

| Heim        | Gast     | Ergebnis |
|-------------|----------|----------|
| Bayern      | Gladbach | 1:3      |
| Druffel     | Bokel    | 0:0      |
| Wiedenbrück | Verl     | 8:1      |

# Bilder

![Ein Bild](img/placeholder.png)

# Spalten

Diese Folie ist in zwei Spalten unterteilt:

:::columns
::::column

Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.

::::
::::column

* Diese
* Aufzählung
* ist
* in
* Spalte 2

::::
:::

**Achtung: Auf dem Handout wird der Text nicht in zwei Spalten angezeigt**

# Code

Ein wenig Code:

``` sql
    SELECT * FROM personen

    SELECT * FROM personen 
    ORDER BY nachname, vorname

    SELECT id, nachname, vorname FROM personen

    SELECT COUNT(*) FROM personen

    SELECT COUNT(*) FROM personen
    WHERE geschlecht='f' AND age < 40
```

# Pausen

Gleich geht es weiter...

\pause

Das war eine Pause.

## Löse die folgenden Aufgaben

* 5 + 8 = \only<3>{13}
* 6 + 2 = \only<3>{8}
* 2 * 6 = \only<3>{12}

# Notizen

Die folgende Notiz ist auf dem Handout, aber nicht in der Präsentation:

\note{
Das ist eine Notiz...
}

# Textausrichtung

\raggedright
Das ist **links**

\center
Das ist **mittig**

\raggedleft
Das ist **rechts**
